# kubernetes-objects

## Deployment and ReplicaSet.

```mermaid
graph TB
    subgraph Deployment
      subgraph ReplicaSet
        Pod1
        Pod2
        Pod3
      end
    end
```

ReplicaSet keeps number of pods.

Deployment help update pods.

## DaemonSet

```mermaid
graph TB
    subgraph DaemonSet
      Pod1
      Pod2
      Pod3
    end
    Pod1 --> Node1
    Pod2 --> Node2
    Pod3 --> Node3
    subgraph Kubernetes Cluster
      Master
      Node1
      Node2
      Node3
    end
```

DaemonSet places one pod per node.

## Persistent Volume and Persistent Volume Claim

```mermaid
graph TB
    ReplicaSet-->PersistentVolumeClaim
    PersistentVolumeClaim-->PersistentVolume
    PersistentVolume-->Volume
    subgraph ReplicaSet
        Pod
        ReplicaSet
    end
    Pod--mount-->Volume
```